# vim
rmdir ~/.vim
rm ~/.vimrc
ln -s ~/myconfigs/home/.vim ~/.vim
ln -s ~/myconfigs/home/.vim/.vimrc ~/.vimrc

# bash
ln -s ~/myconfigs/home/.bash_profile ~/.bash_profile
ln -s ~/myconfigs/home/.bashrc ~/.bashrc

# bin/
rm ~/bin
ln -s ~/myconfigs/home/bin/ ~/bin

# lldb
rmdir ~/.lldb
ln -s ~/myconfigs/home/.lldb ~/.lldb
ln -s ~/myconfigs/home/.lldbinit ~/.lldbinit

# X11
ln -s ~/myconfigs/home/.xinitrc ~/.xinitrc
ln -s ~/myconfigs/home/.Xresources ~/.Xresources


# git
ln -s ~/myconfigs/home/.gitignore ~/.gitignore
# tmux
ln -s ~/myconfigs/home/.tmux ~/.tmux

if [ "$(uname)" == "Darwin" ]; then
   ln -s ~/myconfigs/home/.gitconfig_macosx ~/.gitconfig
   ln -s ~/myconfigs/home/.tmux_macosx.conf ~/.tmux.conf
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
   ln -s ~/myconfigs/home/.gitconfig_linux ~/.gitconfig
   ln -s ~/myconfigs/home/.tmux_linux.conf ~/.tmux.conf
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
   # Do something under 32 bits Windows NT platform
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
   # Do something under 64 bits Windows NT platform
fi
