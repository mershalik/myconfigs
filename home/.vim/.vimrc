" An vimrc file.
"
" To use it, copy it to
"     for Unix and OS/2:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"  for MS-DOS and Win32:  $VIM\_vimrc
"	    for OpenVMS:  sys$login:.vimrc

" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

call plug#begin('~/.vim/plugged')
" Make sure you use single quotes

" Plug 'ervandew/supertab'
Plug 'ycm-core/YouCompleteMe'

" Group dependencies, vim-snippets depends on ultisnips
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

Plug 'airblade/vim-gitgutter'
Plug 'altercation/vim-colors-solarized'
Plug 'powerline/powerline'
Plug 'vim-scripts/taglist.vim'
" Plug 'tyru/open-browser.vim'
" Plug 'previm/previm'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'

" Add plugins to &runtimepath
call plug#end()

" set macatsui enc=utf-8 gfn=Monaco:h15
" set nomacatsui anti enc=utf-8 termencoding=macroman gfn=Courier\ New:h15

behave mswin

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching

set ignorecase
set smartcase

set shiftwidth=0
set tabstop=4
set softtabstop=0
set noexpandtab
set nowrap

:filetype plugin on

" set foldmarker={,}
" set foldmethod=marker
" set foldtext=getline(v:foldstart)

if has("gui_running")
    set guioptions-=T
    set guioptions+=e
    " set t_Co=256
    set guitablabel=%M\ %t
    set guifont=Monospace\ 16
endif

set backup
set undofile

if has("mac") || has("macunix")
   nmap <D-j> <M-j>
   nmap <D-k> <M-k>
   vmap <D-j> <M-j>
   vmap <D-k> <M-k>
endif

if has("mac") || has("macunix") || has("unix")
   set backupdir=~/.vim_BackupDir
   set undodir=~/.vim_BackupDir
elseif has("win32") || has("win16")
   set backupdir=C:\VimBackupDir
   set undodir=C:\VimBackupDir
   set shell=C:\WINDOWS\system32\cmd.exe
   set shellslash
   set shellquote="
   set shellxquote="
   set shellcmdflag=/c
endif

" set hidden      " this allows to leave unwritten buffer. I don't like this set!
set autowriteall
set confirm
let mapleader = ","

" map <F2> :.w !pbcopy<CR><CR>
" map <F3> :r !pbpaste<CR> 

" this macro on letter 'h' is good to edit bin file converted
" to txt with xxd utility. This macro will take 4 digits of
" address, store them in 'z' and use it to substitue whatever
" values will be. It works on the following line:
" 00000140: 5555 5555 5555 5555 5555 5555 5555 5555 UUUUUUUUUUUUUUUU
" it will be converted into
" 00000140: 0140 0140 0140 0140 0140 0140 0140 0140 UUUUUUUUUUUUUUUU
" xxd file.bin file.txt
" xxd -r file.txt file.bin - for reverse
let @h='^lllvllll"zyeewde"zPwde"zPwde"zPwde"zPwde"zPwde"zPwde"zPwde"zPj'

": For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" Don't use Ex mode, use Q for formatting
map Q gq

" This is an alternative that also works in block mode, but the deleted
" text is lost and it only works for putting the current register.
"vnoremap p "_dp

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " Re-read .vimrc when it is written
  autocmd BufWritePost ~/.vimrc     so ~/.vimrc | exec "normal zv"
  autocmd BufWritePost _vimrc     so $VIM\_vimrc | exec "normal zv"

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal g`\"" |
    \ endif

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")

"autocmd BufEnter * if &filetype == "c" | set ff=dos | endif
"autocmd BufEnter * if &filetype == "cpp" | set ff=dos | endif

nnoremap ' `
nnoremap ` '
nnoremap <C-e> 3<C-e>
nnoremap <C-y> 3<C-y>
nmap <silent> <leader>n :silent :nohlsearch<CR>
set listchars=tab:>-,trail:�,eol:$
nmap <silent> <leader>t :set nolist!<CR>


nmap <Leader>q :cs kill -1<CR>
nmap <Leader>W :!./invokeCscopeGlobal.sh<CR> :cs add cscope_global.out<CR><CR>
nmap <Leader>w :cs add cscope_global.out<CR><CR>
nmap <Leader>A :!./invokeCscopeLocal.sh<CR> :cs add cscope_local.out<CR><CR>
nmap <Leader>a :cs add cscope_local.out<CR><CR>

"nmap <C-_>b :csc k 0 <CR>:!cscope -bR<CR>:!ctags -R<CR>:cscope add cscope.out<CR>

nmap <Leader>s :cs find s <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>g :cs find g <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>c :cs find c <C-R>=expand("<cword>")<CR><CR>
"nmap <Leader>t :cs find t <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>e :cs find e <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
nmap <Leader>i :cs find i <C-R>=expand("<cfile>")<CR><CR>
nmap <Leader>d :cs find d <C-R>=expand("<cword>")<CR><CR>

map <C-s>  :cstag <C-R>=expand("<cword>")<CR><CR>

if has("mac") || has("macunix") || has("unix")
   function! g:GitPush()
      call system("find -L . -iname *.[ch] > cscope.files")
      call system("find -L . -iname *.cpp >> cscope.files")
      call system("find -L . -iname *.mm  >> cscope.files")
      call system("cscope -b -q -k")
   endfunction
   nmap <Leader>c :!~/.vim/invokeCsc<CR> :cs reset<CR><CR>
elseif has("win32") || has("win16")
   nmap <Leader>c :csc k 0<CR>:!build_cscope_db.cmd<CR> :csc a cscope.out<CR>
   "   call system("find Build common Core Diag Hardware Include Interface RAP ROM rw sim SSDrw -name "*.[chai]" > all.files")
   function! g:GitPush()
      call system("echo ../../BuildOutput/Modules/flags.h >> all.files")
      call system("echo ../../BuildIFW/Modules/flags.h >> all.files")
      call system("echo ../../BuildMFW/Modules/flags.h >> all.files")
      call system("cscope -b -i all.files")
      call system("csc a cscope.out")
   endfunction
endif

nnoremap <leader>u :call g:GitPush()<CR>

" ------------------------------------------------------------------
" Automatically opens cscope.out if it's available in current dir.
" ------------------------------------------------------------------
if has("cscope")
"    set csprg=/usr/bin/cscope
    set csto=0
    set cst
    set nocsverb
    " add any database in current directory
    if filereadable("cscope.out")
	cs add cscope.out
    " else add database pointed to by environment
    elseif $CSCOPE_DB != ""
        cs add $CSCOPE_DB
    endif
    set csverb
endif
" ------------------------------------------------------------------

" ------------------------------------------------------------------
" I guess the following function and it's use is for Java.
" ------------------------------------------------------------------
function! Foldsearch(search)
        normal zE          "erase all folds to begin with
        normal G$          "move to the end of the file
let folded = 0     "flag to set when a fold is found
        let flags = "w"    "allow wrapping in the search
        let line1 =  0     "set marker for beginning of fold
        while search(a:search, flags) > 0
                let  line2 = line(".")
"echo "pattern found at line # " line2
                if (line2 -1 > line1)
                        "echo line1 . ":" . (line2-1)
                        "echo "A fold goes here."
                        execute ":" . line1 . "," . (line2-1) . "fold"
let folded = 1       "at least one fold has been found
                endif
                let line1 = line2     "update marker
                let flags = "W"       "turn off wrapping
        endwhile
" Now create the last fold which goes to the end of the file.
        normal $G
        let  line2 = line(".")
"echo "end of file found at line # " line2
        if (line2  > line1 && folded == 1)
                "echo line1 . ":" . line2
                "echo "A fold goes here."
                execute ":". line1 . "," . line2 . "fold"
        endif
endfunction

" Command is executed as ':Fs pattern'"
command! -nargs=+ -complete=command Fs call Foldsearch(<q-args>)
" View the methods and variables in a java source file."
command! Japi Fs public\|protected\|private
" ------------------------------------------------------------------

" ------------------------------------------------------------------
" I guess the following is for folding/unfolding.
" ------------------------------------------------------------------
:syn region myFold start="{" end="}" transparent fold
:syn sync fromstart
:syn region Comment start="/\*" end="\*/" fold
:map <A-a> za
" ------------------------------------------------------------------

" ------------------------------------------------------------------
" TagList Plugin Config
" ------------------------------------------------------------------
"filetype plugin on
let g:Tlist_Inc_Winwidth=33
let g:Tlist_Exit_OnlyWindow=1
let g:Tlist_Auto_Open=1
:nn <F8> TlistToggle
" ------------------------------------------------------------------

" ------------------------------------------------------------------
" Solarized Colorscheme Config
" ------------------------------------------------------------------
let g:solarized_contrast="high"    "default value is normal
let g:solarized_visibility="high"    "default value is normal
syntax enable
set background=dark
colorscheme solarized
" ------------------------------------------------------------------

" ------------------------------------------------------------------
" CtrlP Config
" ------------------------------------------------------------------
let g:ctrlp_mruf_case_sensitive = 1
let g:ctrlp_by_filename=1
let g:ctrlp_regexp=1

" Set no max file limit
let g:ctrlp_max_files = 0
" Search from current directory instead of project root
let g:ctrlp_working_path_mode = 0

" ------------------------------------------------------------------

" au BufWinLeave * silent mkview
" au BufWinEnter * silent loadview

" ------------------------------------------------------------------

" let s:powerline_pycmd = substitute(get(g:, 'powerline_pycmd', has('python') ? 'py' : 'py3'), '\v^(py)%[thon](3?)$', '\1\2', '')
if has("mac") || has("macunix")
let g:powerline_pycmd="py"
set rtp+=~/Library/Python/2.7/lib/python/site-packages/powerline/bindings/vim
else
let g:powerline_pycmd="py3"
set rtp+=/usr/lib/python3.8/site-packages/powerline/bindings/vim/
endif
set laststatus=2
set showtabline=2
set noshowmode

let g:ycm_confirm_extra_conf = 0

" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'


" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<C-k>"
let g:UltiSnipsJumpForwardTrigger = "<C-n>"
let g:UltiSnipsJumpBackwardTrigger = "<C-p>"

augroup PrevimSettings
   autocmd!
   autocmd BufNewFile,BufRead *.{md,mdwn,mkd,mkdn,mark*} set filetype=markdown
augroup END

let g:previm_open_cmd = 'firefox --new-window'

" Use 256 colours (Use this setting only if your terminal supports 256 colours)
set guifont=Inconsolata\ for\ Powerline:h18
let g:Powerline_symbols = 'fancy'
set t_Co=256
set fillchars+=stl:\ ,stlnc:\
" set term=xterm-256color
set termencoding=utf-8
set encoding=utf-8
