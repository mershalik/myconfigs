#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

set -o vi

# MacPorts Installer addition on 2018-10-04_at_18:34:27: adding an appropriate PATH variable for use with MacPorts.
# MacPorts added to .profile but it didn't seem to work there. I moved it here.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.

alias ls='ls -G'
alias ll='ls -alG'

PS1='[\u@\h \W]\$ '
function _update_ps1() {
    PS1=$(powerline-shell $?)
}
if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
fi

powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
if [[ $TERM == linux ]]; then
. /usr/lib/python3.7/site-packages/powerline/bindings/bash/powerline.sh
else
. ~/Library/Python/2.7/lib/python/site-packages/powerline/bindings/bash/powerline.sh 
. /opt/local/share/git/contrib/completion/git-completion.bash
fi
