#! /bin/bash 

# to quit on any errors, including in functions
#set -e
# to quit on errors in pipes too.
set -o pipefail

P4port=ssl:tco.perforce.firmware.seagate.com:8667
P4user=yury.meshkov

# git status output string changed from "working directory clean" to ... tree ... somewhere near release 
EmptyRepoGitMessage="working tree clean"

# Clean completion with deletion of all the created tmp files
function BailOut
{
   GetCurrentGitBranch

   #echo started with $InitialGitBranch and now $CurrentGitBranch
   # Switch back to the local git branch as it was before the script run. 
   if [ -n "$InitialGitBranch" ] && [ -n "$CurrentGitBranch" ]
   then
      if [ $InitialGitBranch != $CurrentGitBranch ]
      then
         #echo Current local git branch is $CurrentGitBranch Returning it to $LocalGitBranch.
         git co $InitialGitBranch
      fi
   fi

   if [[ ( "$importedChangelistsCounter" > 0 ) ]]
   then
      echo $importedChangelistsCounter changelists were imported. Please, don\'t forget to run git rebase perforce.
   fi

   if [ -f $tmpFileName4CLInfo ]
   then
      rm $tmpFileName4CLInfo
   fi

   if [ -f $tmpFileName4CommitMessage ]
   then
      rm $tmpFileName4CommitMessage
   fi

   if [ -f $p4_client_view_file ]
   then
      rm $p4_client_view_file
   fi

   if [ -f $p4ClientCurrConf ]
   then
      # restore workspace to not have StreamAtChange setting.
      p4 client -i < $p4ClientCurrConf 1>/dev/null 2>/dev/null
      rm $p4ClientCurrConf
   fi

   exit 1
}

# This function calls git with the parameters it recieved and searches for the string
# $ExpectedOutputString in the git output.
# It returns 0 if the string is found and 1 otherwise. This is a little non-intuitive
# because bash "if" operator will execute "then" portion on returned 0 as "success".
function CallGitCmd()
{
   retValue=$(git $@ | awk 'BEGIN {ec=1} /'"$ExpectedOutputString"'/ {ec=0; exit} END {print ec}')
   if [ $? -ne 0 ]
   then
      echo Fatal error calling git. Quitting.
      BailOut
   fi
   return $retValue
}

# This function reads into P4CommitMessage a commit message from Perfoce but with some format modifications like
# it removes date and part of the commiter name after @ whichs is workspace name.
function GetFromP4CommitMessage()
{
   p4 changes -m1 $depot@$changelist | awk 'BEGIN {ec=0} {if ($1=="Change") ec=$2; exit} END {print ec}' > $tmpFileName4CommitMessage

   if [ $? -ne 0 ]
   then
      echo Fatal error calling p4 changes. Quitting.
      BailOut
   fi

   P4Changelist=$(cat $tmpFileName4CommitMessage)

   if [ $MappingStreamsChangeDetected -ne 0 ]
   then
      MessageMarker="~s~ "
   else
      if [ $changelist != $P4Changelist ]
      then
         MessageMarker="  - "
      else
         MessageMarker="--- "
      fi
   fi

   p4 changes -m1 -L @$changelist | tr -d "\t" | awk '
   {
      split( "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec", month, " " );
      line++;
      if ( line == 1 )
         {
            split($4,date,"/");
            split($6,name,"@");
            # Convert string date to int dateDig
            dateMon=date[2]+0;
            dateDig=date[3]+0;
            printf( $2 " on " dateDig " " month[dateMon] " by " name[1] " \"" ); 
         }
      if ( line == 3 ) { len=length($0); if (substr($0,length($0)) == "\r") len=len-1; print substr($0,1,len) "\"" }
   }' > $tmpFileName4CommitMessage
   P4CommitMessage=$MessageMarker$(cat $tmpFileName4CommitMessage)
}

# sets StreamAtChange to provided parameter
function SetStreamAtChange()
{
   cp $p4ClientCurrConf $p4ClientConf2Set
   echo "StreamAtChange: " $@ >> $p4ClientConf2Set
   p4 client -i < $p4ClientConf2Set 1>/dev/null 2>/dev/null
   if [ $? -ne 0 ]
   then
      echo 
      echo "  ->Fatal error calling p4 client -i."
      echo "  -->This usually means Perforce won\'t allow you to set StreamAtChange via GUI as well."
      echo "  --->Quitting."
      BailOut
   fi

   if [ -f $p4ClientConf2Set ]
   then
      rm $p4ClientConf2Set
   fi
}

# Create .p4client file. It holds a list of all depotPaths imported by the stream. This function also performs comparison
# of mapping streams with the previously created .p4client file
function AskP4ForMappingStreams()
{
   # Ask p4 client to output to stdout and ignore output until View: sections begins
   p4 client -o | awk 'BEGIN {toggle=0} {if ($1=="View:") toggle=1; if (toggle==1 && $1!="View:") print $1}' > $p4_client_view_file.new

   # Check that .p4client file exist. If yes, rename it. The only reason it's been kept between
   # this script invokation is to detect that mapping Streams have been changed.
   if [ -f $p4_client_view_file ]
   then
      # Compare just created .p4client.new against old one and count differenceis in lines
      difference=`diff $p4_client_view_file $p4_client_view_file.new | wc -l`
      if [ $difference -ne 0 ]
      then
         echo ----------------------------------------------
         diff $p4_client_view_file $p4_client_view_file.new
         echo ----------------------------------------------
         MappingStreamsChangeDetected=1
      fi
   fi
   mv $p4_client_view_file.new $p4_client_view_file
}

# This function will synch all mapped depotPaths to a given $changelist one by one.
function SyncPerforcetoChangelist()
{
   NumOfUpdatedFiles4CL=0

   # This may change mapping list on Perforce
   SetStreamAtChange $changelist

   MappingStreamsChangeDetected=0
   # this will get mapping list from Perforce. The mapping list is only used to detect
   # a change in streams. The change is indicated in a generated git commit message.
   AskP4ForMappingStreams

   p4 sync $root@$changelist 2>/dev/null | wc -l >$tmpFileName4CommitMessage
   NumOfUpdatedFiles=$(cat $tmpFileName4CommitMessage)
   echo -ne " $NumOfUpdatedFiles file(s) updated."
}

function GetCurrentlySyncedPerforceCL()
{
   # This gets CL synced to workspace from Perforce which is wrong often
   #p4 changes -m1 @$p4client | awk 'BEGIN {ec=0} {if ($1=="Change") ec=$2; exit} END {print ec}' > $tmpFileName4CLInfo

   # Ask git for the CL synced to workspace from Perforce
   git log -2 --oneline perforce -- | awk '{print $3}' > $tmpFileName4CLInfo

   if [ $? -ne 0 ]
   then
      echo Fatal error calling p4 to get currently synced CL. Quitting.
      BailOut
   fi

   # This loop would return previous CL if the latest commit does not have CL number in its message
   while
      read number
   do
      if [ "$number" -eq "$number" ] 2>/dev/null;
      then
         CurrentCL=$number
         break
      fi
   done < $tmpFileName4CLInfo
}

#This function will create a list of CLs from $CurrentCL till @now in all mapped depotPaths
function GetCurrentCL2NowList()
{
   # Going to be adding to this file. Make sure it's empty.
   if [ -f $tmpFileName4CommitMessage ]
   then
      rm $tmpFileName4CommitMessage
   fi

# Get the list of newer CLs. Bailout if there is nothing to sync to.
   p4 changes -s submitted $root@$CurrentCL,@now | awk '{if ($1 == "Change") print $2}' | sort -g -u >> $tmpFileName4CommitMessage
   if [ $? -ne 0 ]
   then
      echo Fatal error calling p4 changes. Quitting.
      BailOut
   fi

   cat $tmpFileName4CommitMessage | sort -g -u > $tmpFileName4CLInfo

   numOfCLs=$(expr $(wc -l < $tmpFileName4CLInfo) - 1)
   if [ $numOfCLs -eq 0 ]
   then
      echo There is nothing to sync. The local workspace is up to date.
      BailOut
   else
      echo There is\(are\) $numOfCLs new changelist\(s\) to be updated from Perforce.
   fi
}

# This function checks if there are any open files in Perforce for edit/add/delete.
# If it is called with "PromptAllowed" and there is at least one opened file in Perforce
# it will prompt user for Y/N and will revert all the files if user replies Y.
function CheckForOpenedFilesInPerforce()
{
   # Check that there are no opened files in perfoce for edit/add/delete. If any they won't be updated.
   # Need to revert them before updating local perforce branch.
   p4 opened 2>&1 | awk '
   {
      if ($1 != "File(s)" || $2 != "not" || $3 != "opened" || $4 != "on" || $5 != "this")
         { split($0, oneline, "#"); print oneline[1]; }
   }' > $tmpFileName4CLInfo
   numOfCLs=$( expr $(wc -l < $tmpFileName4CLInfo) )
   if [ $numOfCLs -eq 0 ]
   then
      return 0
   else
      if [ $@ == "PromptAllowed" ]
      then
         echo There is\(are\) $numOfCLs opened files for edit/add/delete in Perforce.
         echo They must be reverted before getting new changelists from Perforce server.
         echo MAKE SURE you won\'t lose your changes! Once Perforce revert a file it will
         echo overwrite it with the latest checked in copy from the server.
         echo Be WARNED and CAREFUL before answering the following question:
         while true; do
            read -p "Do you want to revert them now? (yes/no)" yn
            case $yn in
               [Yy]* ) userReply=true; break;
                  ;;
               [Nn]* ) userReply=false; break;
                  ;;
               *) echo Please reply Y or N only;;
            esac
         done
         if [ $userReply == "true" ]
         then
            while
               read filename
            do
               echo reverting $filename
               p4 revert $filename | awk '{}'
            done < $tmpFileName4CLInfo
         fi
      fi
      return 1
   fi
}

# Returns current git branch in CurrentGitBranch variable.
function GetCurrentGitBranch()
{
   git br | awk '{if ($1=="*") print $2}' > $tmpFileName4CLInfo
   if [ $? -ne 0 ]
   then
      echo Fatal error trying to read current git branch. Quitting.
      BailOut
   fi
   CurrentGitBranch=$(cat $tmpFileName4CLInfo)
}

# This function will make sure git workspace is clean with no any changed files befoe proceeding.
# Then it will get newer Perforce changelists one by one and commit them into git branch "perforce" 
# with the same commit messages as in Perforce.
function PullFromPerforce()
{
   echo Checking for any pending changes.
   # Test git status for any pending changes. Working directory must be clean to proceed.
   ExpectedOutputString=$EmptyRepoGitMessage
   if ! CallGitCmd "status" 
   then
      echo Please make your working area clean before updating it. You may want to use git stash command.
      BailOut
   fi

   # Check that there are no open files in Perforce for edit/add/delete. This check is needed because
   # Perforce will NOT update any open files. All open files must be reverted before proceeding with the 
   # pulling of new changelists from Perfoce to local branch perforce.
   if ! CheckForOpenedFilesInPerforce "PromptAllowed"
   then
      if ! CheckForOpenedFilesInPerforce "PromptNotAllowed"
      then
         echo Can\'t proceed while there are opened files for edit/add/delete in Perforce.
         BailOut
      fi
      # Test git status for any pending changes again in case reverting Perforce files left
      # some files in a dirty state.
      ExpectedOutputString=$EmptyRepoGitMessage
      if ! CallGitCmd "status" 
      then
         # Perforce overwrote local files with their latest checked in versions from Perforce repo.
         # It is not desired at this point. We'll use git to restore the changed files from git repo.
         git checkout -- .
      fi
      # Check if we were able to make current git status clean. 
      ExpectedOutputString=$EmptyRepoGitMessage
      if ! CallGitCmd "status" 
      then
         echo The working directorey is not clean. Please, resolve this situation.
         BailOut
      fi
   fi

   GetCurrentGitBranch

   InitialGitBranch=$CurrentGitBranch

   if [ $InitialGitBranch == "(no" ]
   then
      echo You are currently NOT on any branch. After script completion the master branch will be left current.
      InitialGitBranch=master
   fi

   # returns in CurrentCL variable
   GetCurrentlySyncedPerforceCL

   if [ "$CurrentCL" -ne 0 ]
       then
          echo -ne Local git "perforce" branch should be synched to the remote Perforce changelist $CurrentCL.
	   echo -ne " Making sure of that."
	   changelist=$CurrentCL
	   SyncPerforcetoChangelist

	   # Check if current branch and Perforce are in sync.
	   ExpectedOutputString=$EmptyRepoGitMessage
	   if ! CallGitCmd "status" 
	   then
	      echo -e '\n\n'The working directorey is not clean. Something is wrong. Local perforce branch
              echo -e and remote Perforce should have all files matching at this point. It is possible
              echo -e that someone changed Perforce mapping streams and things endup like this. This is a bad
              echo -ne behaviour on Perforce\' part. You can do:'\n' -\> git add .'\n' -\> git ci --amend -C HEAD'\n'
              echo -e to pack all the differencies into the previous CL and repeat pulling.
	      BailOut
	   fi
   fi
   echo .

   GetCurrentCL2NowList

   if [ $InitialGitBranch != $RemotePerforceBranch ]
   then
      #echo Current local git branch is $LocalGitBranch. Switching to $RemotePerforceBranch
      git co $RemotePerforceBranch
      # Check that now the current branch is as we switched it to.
      ExpectedOutputString="* $RemotePerforceBranch"
      if ! CallGitCmd "br"
      then
         echo Something went wrong. I didn\'t managed to switch local git branch to $RemotePerforceBranch
         BailOut
      fi
   else
      echo branch is already remote.
   fi

# Entering loop. Once iteration for each CL.
   while
      read changelist
   do
      if [ -z $changelist ]
      then
         break
      fi

      if [ "$FORCE" != "1" ]
      then
         if [ $changelist -eq $CurrentCL ]
         then
            # Sanity check. The commit messages of the HEAD of local "perforce" branch that is tracking the remote Perforce server must match
            # the commit message of the Perforce changelist it is synced to. $CurrentCL
            # awk is needed below to make line endings same for git and p4 outputs. Without awk one uses 0d0a and another 0a0a,
            # causing comparison mismatch on the same string.
            git log -1 --pretty=%B | awk '{print $2}' > $tmpFileName4CommitMessage
            GitCommitCL=$(cat $tmpFileName4CommitMessage)

            GetFromP4CommitMessage

            # extract only CL number from Perforce commit message
            P4CL=$(echo $P4CommitMessage | awk '{print $2}' )

            if [ "$GitCommitCL" == "$P4CL" ]
            then
               continue
            else
               echo Sanity check error. Git commit message:
               git log -1 --pretty=%B | awk '{print $0}'
               echo does not match Perforce commit message:
               echo "   $P4CommitMessage"
               echo This may mean that git repo is NOT in sync with Perforce! You may pass --force to skip this check
               BailOut
            fi
         fi
      fi
      echo -ne Syncing to CL $changelist...

      SyncPerforcetoChangelist

      GetFromP4CommitMessage

      git add -A
      git commit -m "$P4CommitMessage" > $tmpFileName4CommitMessage
      rm $tmpFileName4CommitMessage

      importedChangelistsCounter=$(expr $importedChangelistsCounter + 1)

      # Test git status for any pending changes. Working directory must be clean to proceed.
      ExpectedOutputString=$EmptyRepoGitMessage
      if CallGitCmd "status"
      then
         echo " Commited into local branch."
      else
         echo " Something is wrong. Git local directory is not clean after Git commit. Quitting."
         BailOut
      fi

   done < $tmpFileName4CLInfo
}

# This function is called when the script is called with parameter 'init'.
# It checks that there is no current git repo present and proceeds to create one.
# Then it adds all files to it and checks them in with the commit message taken
# from the currnt p4 changelist commit message.
function InitGit()
{
   # Check that now the current branch is as we switched it to.
   ExpectedOutputString="Initialized empty Git repository"
   if CallGitCmd "init"
   then
      echo Successfully created new empty Git repository.
   else
      echo Didn\'t create new git repo. Does it exist already?
      BailOut
   fi

   # Create .p4config file if it does not exist yet
   p4_config_file=.p4config
   if [ ! -f $p4_config_file ]
   then
      echo "P4PORT="$P4port > $p4_config_file
      echo "P4USER="$P4user >> $p4_config_file
      echo "P4CLIENT="$(basename `pwd`) >> $p4_config_file
      InitGlobals
   fi

   # THis isues Perforce knowledge of the last synched CL to workspace.
   p4 changes -m1 @$p4client | awk 'BEGIN {ec=0} {if ($1=="Change") ec=$2; exit} END {print ec}' > $tmpFileName4CLInfo
   read changelist < $tmpFileName4CLInfo
   rm $tmpFileName4CLInfo

   GetFromP4CommitMessage

   echo Adding all files to git repo. This may take awhile.
   git add .
   git commit -m "$P4CommitMessage" > $tmpFileName4CommitMessage
   rm $tmpFileName4CommitMessage

   git co -b perforce
   git br -d master
}

# This function will get all the git staged files. And convert each of them into an appropriate p4 command and execute it.
function SendOutToPerforce
{
   pushd . >/dev/null
   # Change to git root directory so that all git diff --name-status file paths will make sense for p4
   cd $(git rev-parse --show-toplevel)

   # Check if an additional parameter to push cmd was provided
   if [ -n "$GivenSHA" ]
   then
      # $GivenSHA is expected in teh form SHA..SHA1.
      git diff $GivenSHA --name-status | awk '
      {
         if ( $1=="A" ) print "p4 add `cygpath -pwa " $2 "`"
         if ( $1=="D" ) print "p4 delete `cygpath -pwa " $2 "`"
         if ( $1=="M" ) print "p4 edit `cygpath -pwa " $2 "`"
      }' > $tmpFileName4CLInfo
   else
      # for new file: p4 add $filename will be created.
      # for  deleted: p4 delete $filename will be created.
      # for modified: p4 edit $filename will be created.
      # cygpath will convert relative cygwin path to absolute Windows path for p4.
      git st | awk ' BEGIN {committedSectionCurrent=0}
      {
         if ( $2=="Changes" && $3=="to" && $4=="be" && $5=="committed:" ) committedSectionCurrent=1
         if ( $2=="Changes" && $3=="not" && $4=="staged" && $5=="for" && $6=="commit:" ) committedSectionCurrent=0
         if ( $2=="Untracked" && $3=="files:" ) committedSectionCurrent=0

         if ( committedSectionCurrent==1 && $2 == "new" && $3 == "file:" ) print "p4 add `cygpath -pwa " $4 "`"
         if ( committedSectionCurrent==1 && $2=="deleted:") print "p4 delete `cygpath -pwa " $3 "`"
         if ( committedSectionCurrent==1 && $2=="modified:") print "p4 edit `cygpath -pwa " $3 "`"
      }' > $tmpFileName4CLInfo
   fi
   if [ $? -ne 0 ]
   then
      echo Fatal error trying to read current git branch while already bailing out. Quitting.
   fi

   numOfAddedDeletedOrModdedFiles=$(wc -l < $tmpFileName4CLInfo)
   if [ $numOfAddedDeletedOrModdedFiles -eq 0 ]
   then
      echo Nothing to send to Perforce since there is nothing staged in git.
   fi

   while
      read GeneratedCmd
   do
      echo ---------------------------------------------------------------------
      eval echo $GeneratedCmd
      eval $GeneratedCmd
   done < $tmpFileName4CLInfo

   popd >/dev/null
}

function ApplyPatch()
{
   if [ -f $PatchFileName ]
   then
      grep '+++' review-117273.diff | awk '{print $2}' > $tmpFileName4CLInfo
      numOfFiles=$( expr $(wc -l < $tmpFileName4CLInfo) )
      echo There are $numOfFiles files in the patch.

      if [ $numOfFiles -eq 0 ]
      then
         return 0
      else
         while
            read filename
         do
            p4 edit $filename | awk '{}'
         done < $tmpFileName4CLInfo

         echo Applying patch.
         patch --no-backup-if-mismatch -p5 < $PatchFileName
         if [ $? -ne 0 ]
         then
            echo .
            echo Patch didn\'t apply successfully. There are some file differencies
            echo between your local files and the files the patch was created with.
            git checkout -- .
            if [ "$FORCE" != "1" ]
            then
               echo You can study *.rej files for clues but they are deleted by default.
               echo You can add -l option to leave *.rej files.
               find SSD-trunk/F1_dev/source -name *.rej | xargs rm
            fi
         fi
      fi
   fi
}

function SyncupAllMappedDepotPaths()
{
   if [ -n "$GivenSHA" ]
   then
      changelist=$GivenSHA
   else
      echo "CL # must be provided".
      BailOut
   fi

   if [ "$FORCE" = "1" ]
   then
      options=-f
   fi

   echo -ne Syncing to CL $changelist...

   SyncPerforcetoChangelist
}

# This function will look for a given file in the current and upper directories.
# It will return true of false if file is found. The folder where file is found
# will be the current.
function LookForFile()
{
   while true; do
      if [ -f $@ ]
      then
         return 1
      else
         cd ..
         if [ `pwd` == "/" ]
         then
            echo Cannot find $@ file.
            exit 1
         fi
      fi
   done
}

function InitGlobals( )
{
   ##############################################################

   tmpFileName4CLInfo=tmp_current_cl_file.swp
   tmpFileName4CommitMessage=tmp_current_commit_message.swp
   p4_client_view_file=.p4client

   # Configuration parameters:
   RemotePerforceBranch=perforce
   importedChangelistsCounter=0
   MappingStreamsChangeDetected=0

   #########################$p4ClientConf2Set#####################################
   pushd . >/dev/null

   # To use this script in Cygwin make sure that at least P4CLIENT is set. Example:
   # p4 set P4CLIENT=ssd-vista
   # This is the content of my .p4config file which is tested to be working:
   #P4PORT=sgp.perforce.firmware.seagate.com:1667
   #P4USER=yury.meshkov
   #P4CLIENT=ssd-vista
   #This will set p4 set P4CONFIG to .p4config file in the current folder.
   p4_config_file=.p4config
   if ! LookForFile $p4_config_file
   then
      p4 set P4CONFIG=`cygpath -pwa $p4_config_file`
   fi
   #
   # your client specification (created with p4 client -c client_name)
   # has "allwrite" and "clobber" options set.
   #

   # Read p4 client into p4client variable for future p4 invocations.
   p4client=$(p4 set P4CLIENT | awk '{split($1,i,"="); print i[2];}')
   if [ -z "$p4client" ]
   then
      echo p4 client name is empty. Cannot proceed further.
   fi 

   # This portion of code delete old p4clint file and issue 'p4 client -o'' cmd to ge new spec.
   # Then it will parse output to get depot variable
   {
      p4ClientCurrConf=.p4clientCurrentConfig
      p4ClientConf2Set=.p4clientConfig2Set
      if [ -f $p4ClientCurrConf ]
      then
         rm $p4ClientCurrConf
      fi
      # Ask p4 client to output to stdout and save it all except StreamAtChange:
      p4 client -o | awk '{if ($1 != "StreamAtChange:") print $0}' > $p4ClientCurrConf
      if [ $? -ne 0 ]
      then
         echo Fatal error calling p4 client to get list of mapped depot paths. Quitting.
         BailOut
      fi

      # extract Stream: //EnterpriseSSD/Stari_trunk styled line
      # some cygwin awk captured the last string in line with trailing 0x0d or \r sign as part of string. So below 'substr' manipulations is to remove that
      depot=$(cat $p4ClientCurrConf | awk '{ if ($1=="Stream:") { len=length($2); if (substr($2,length($2)) == "\r") len=len-1; print substr($2,1,len)"/..." } }')
      root=$(cat $p4ClientCurrConf | awk '{ if ($1=="Root:") { len=length($2); if (substr($2,length($2)) == "\r") len=len-1; print substr($2,1,len)"\\..." } }')
   }

   echo "    Configuration:"
   echo "      1 p4client=$p4client"
   echo "      2 depot=$depot"
   echo "      3 root=$root"

   popd >/dev/null
}

#
# So called main( ) function is below this line. Execution starts below this line. Before this line are function definitions.
#
# Start with parsing the arguments
while [[ $# > 0 ]]
do
   key="$1"
   shift

   case $key in
      -l|-f|--force)
         FORCE=1
         ;;
      init)
         Action=Init
         ;;
      pull)
         Action=Pull
         ;;
      push)
         Action=Push
         ;;
      patch)
         Action=Patch
         ;;
      sync)
         Action=Sync
         ;;
#      -s|--searchpath)
#         SEARCHPATH="$1"
#         shift
#         ;;
#      --default)
#         DEFAULT=YES
#         ;;
      *)
         # unknown option
         GivenSHA=$key
         PatchFileName=$key
         ;;
   esac
done

# Here depending on the value of$Action different functionality of this script will be called.
case $Action in
   Init)
      InitGit
      ;;
   Pull)
      InitGlobals
      PullFromPerforce
      ;;
   Push)
      InitGlobals
      SendOutToPerforce
      ;;
   Patch)
      InitGlobals
      ApplyPatch
      ;;
   Sync)
      InitGlobals
      SyncupAllMappedDepotPaths
      ;;
   *)
      # unknown option
      echo "`basename $0` version 0.4"
      echo "please call it as `basename $0` [--force] <command> <param>"
      echo " Accepted commands:"
      echo "  init  - This will create a new git repo in the current folder and check in All files"
      echo "          into it with the commit message taken from Perforce currently sync-ed changelist."
      echo "  patch - This will attempt to apply <param> patch. If not successful it will revert all the changes." 
      echo "  pull  - This will import all the new changelists from Perforce server into local branch"
      echo "          called \"perforce\". The latest git commit message will be ensured to match Perforce"
      echo "          changelist\'s commit message for sanity check. This is done to make sure that the"
      echo "          local branch \"perforce\" is closely following remote Perforce server without"
      echo "          any local commits and has all the changelists. The check maybe surpressed with --force."
      echo "  push  - Will call p4 edit/add/delete commands on all a set of files. Convenient for preparing"
      echo "          to check in the changes into Perforce server. By default, when to additional parameters"
      echo "          provided the push operation will be done on all staged files. If additional parameter"
      echo "          provided it should be in the form of SHA1..SHA2. Then p4 edit/add/delete will be performed"
      echo "          on the files in the given commit(s). Example: `basename $0` push 17946^..17946"
      echo "  sync  - Calls p4 sync on all mapped paths to a given CL. It can be combined with -f (be prepared to wait)"
      echo "  help  - This help message."
      echo " Optional parameters:"
      echo "  -f or --force - Will skip sanity check on equality of latest git commit message in \"perforce\""
      echo "         branch and Perforce commit message of the changelist it was synced to."
      ;;
esac

# Clean up before completion.
BailOut
