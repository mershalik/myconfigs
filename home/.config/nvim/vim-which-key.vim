
nnoremap <silent> <leader> :WhichKey '<Space>'<CR>
nnoremap <silent> <localleader> :WhichKey ','<CR>


" By default timeoutlen is 1000 ms
set timeoutlen=500
