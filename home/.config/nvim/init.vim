" Neovim's rc file.
"
call plug#begin('~/.config/nvim/plugged')
" Make sure you use single quotes

Plug 'neoclide/coc.nvim', {'branch':'release'}

Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'ryanoasis/vim-devicons'

Plug 'liuchengxu/vim-which-key'

Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'

Plug 'rbgrouleff/bclose.vim'
Plug 'francoiscabrol/ranger.vim'

Plug 'nvim-treesitter/nvim-treesitter'
Plug 'honza/vim-snippets'
Plug 'yegappan/taglist'

" I did not notice differences in coloring cpp files after adding below
" plugins hence commented them out:
Plug 'octol/vim-cpp-enhanced-highlight'
" Plug 'sheerun/vim-polyglot'
" Plug 'vim-jp/vim-cpp'

" The below are colorschemes
Plug 'joshdick/onedark.vim'
Plug 'morhetz/gruvbox'
Plug 'altercation/vim-colors-solarized'

" plugin to move around quickly. Starts with s and then two more characters
Plug 'justinmk/vim-sneak'

Plug 'airblade/vim-gitgutter'

" Initialize plugin system
call plug#end()

" Map the leader key to SPACE
" let mapleader="\<SPACE>"
let g:mapleader = "\<Space>"
let g:maplocalleader = ','

source $HOME/.config/nvim/coc.vim
source $HOME/.config/nvim/vim-which-key.vim
source $HOME/.config/nvim/ranger.vim
source $HOME/.config/nvim/git-gutter.vim
source $HOME/.config/nvim/statusline.vim
source $HOME/.config/nvim/treesitter.vim

set showmatch           " Show matching brackets.
set rnu
set formatoptions+=o    " Continue comment marker in new lines.
set expandtab           " Insert spaces when TAB is pressed.
set tabstop=4           " Render TABs using this many spaces.
set shiftwidth=4        " Indentation amount for < and > commands.

set nojoinspaces        " Prevents inserting two spaces after punctuation on a join (J)

set cursorline          " Highlits line that cursor is on.

" More natural splits
set splitbelow          " Horizontal split below current.
set splitright          " Vertical split to right of current.

if !&scrolloff
	set scrolloff=3       " Show next 3 lines while scrolling.
endif
if !&sidescrolloff
	set sidescrolloff=5   " Show next 5 columns while side-scrolling.
endif
set nostartofline       " Do not jump to first character with page commands.


" Tell Vim which characters to show for expanded TABs,
" trailing whitespace, and end-of-lines. VERY useful!
if &listchars ==# 'eol:$'
  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif

set backup
set undofile
set backupdir=~/.backupdir_nvim
set undodir=~/.backupdir_nvim

colorscheme solarized

" Also highlight all tabs and trailing whitespace characters.
highlight ExtraWhitespace ctermfg=darkgreen guifg=darkblue
match ExtraWhitespace /\s\+$\|\t/

" make signcolumn less prominent
highlight SignColumn guibg=black ctermbg=black

set ignorecase          " Make searching case insensitive
set smartcase           " ... unless the query has capital letters.
set gdefault            " Use 'g' flag by default with :s/foo/bar/.

" Use <C-L> to clear the highlighting of :set hlsearch.
if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<CR><C-L>
endif

" Relative numbering
function! ListToggle()
  if(&list == 1)
    set nolist
  else
    set list
  endif
endfunc
" Toggle between normal and relative numbering.
nnoremap <leader>l :call ListToggle()<cr>


" For sneak.vim - turn ON label-mode for a minimalist alternative to EasyMotion:
let g:sneak#label = 1

" cscope section to complement coc.nvim
nmap <Leader><Leader>q :cs kill -1<CR>
nmap <Leader><Leader>O :!./invokeCscopeGlobal.sh<CR> :cs add cscope_global.out<CR><CR>
nmap <Leader><Leader>o :!./invokeCscopeLocal.sh<CR> :cs add cscope_local.out<CR><CR>
nmap <Leader><Leader>s :cs find s <C-R>=expand("<cword>")<CR><CR>
nmap <Leader><Leader>g :cs find g <C-R>=expand("<cword>")<CR><CR>
nmap <Leader><Leader>c :cs find c <C-R>=expand("<cword>")<CR><CR>
nmap <Leader><Leader>t :cs find t <C-R>=expand("<cword>")<CR><CR>
nmap <Leader><Leader>e :cs find e <C-R>=expand("<cword>")<CR><CR>
nmap <Leader><Leader>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
nmap <Leader><Leader>i :cs find i <C-R>=expand("<cfile>")<CR><CR>
nmap <Leader><Leader>d :cs find d <C-R>=expand("<cword>")<CR><CR>

" To quickly switch between .cpp and .h files.
nnoremap <leader><leader>h :e %:p:s,.h$,.X123X,:s,.cpp$,.h,:s,.X123X$,.cpp,<CR>

" Search and Replace
" nmap <Leader><Leader>s :%s//g<Left><Left>

" Fuzzy Finder
nmap <Leader>ff :FZF<CR>

let g:NERDTreeWinPos = "right"
nmap <C-n> :NERDTreeToggle<CR>
